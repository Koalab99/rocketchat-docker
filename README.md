# rocketchat-docker

A set of useful files for making rocketchat + mongodb docker containers using docker-compose.

## Installation
### Setup your environment
First you need to have docker and docker-compose installed on your machine.
You can find how on [the docker documentation](https://docs.docker.com/install/).
Then you need to clone this repository with:
```shell
git clone https://gitlab.com/Koalab99/rocketchat-docker
```
Then change your directory
```shell
cd rocketchat-docker
```
### Start the docker daemon
There is two ways of activating the docker daemon.
#### Starting it each time you need it
You will need to start it everytime you want to use docker. It resets when you shutdown the machine.
```shell
systemctl start docker
```
to reverse this operation, type
```shell
systemctl stop docker
```
#### Starting it at bootup
```shell
systemctl enable docker
systemctl start docker
```
to reverse this operation, type
```shell
systemctl disable docker
systemctl stop docker
```

### Configuration
Coming soon

### Initialize
Run the following command
```shell
docker-compose up -d
```
Wait for sometimes and you can see rocketchat in a browser at URL http://localhost

### Restart a previously made services
```shell
docker-compose start
```

